<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterventionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intervention', function (Blueprint $table) {
            $table->increments('id');
            $table->date('profile_date');
            $table->integer('pd_id');
            $table->integer('pdv_id');
            $table->integer('md_id');
            $table->integer('mdv_id');
            $table->integer('pt_id');
            $table->integer('ptd_id');
            $table->integer('ptdv_id');
            $table->text("summary");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('intervention');
    }
}

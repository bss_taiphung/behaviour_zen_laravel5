<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment', function (Blueprint $table) {
            $table->increments('id');
            $table->time('time_started');
            $table->integer('record_id');
            $table->integer('at_id');
            $table->integer('atd_id');
            $table->integer('atdv_id');
            $table->integer('fd_id');
            $table->integer('fdv_id');
            $table->text('summary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assessment');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: nasterblue
 * Date: 11/13/15
 * Time: 5:05 PM
 */

$happiAPICode = array(
    //create user begin
    'code_unique_email' =>'Duplicate email',
    //create user end
    //name size begin
    'code_name'    => 'Name\'s required',
    //name size end
    //password rule begin
    'code_password'     => 'Password is not valid!',
    //password rule end

);
$apiErrorCodes = array(
    //create user begin
    'code_unique_email' => 1000,
    //create user end
    //name size begin
    'code_name'    => 1500,
    //name size end
    //password rule begin
    'code_password'     => 1300,
    //password rule end

);
$happiAPICode['ApiErrorCodes'] = $apiErrorCodes;
$apiErrorCodesFlip = array_flip($apiErrorCodes);
$happiAPICode['ApiErrorCodesFlip'] = $apiErrorCodesFlip;

return $happiAPICode;

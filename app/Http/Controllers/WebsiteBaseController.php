<?php namespace Http\Controllers;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Routing\Controller;
use Illuminate\Contracts\Validation;
use Illuminate\Http\Request;

abstract class WebsiteBaseController extends Controller
{

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->validator = new Validation();
    }

}

<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Common\ErrorFormat;
use Illuminate\Routing\Controller;
use Illuminate\Contracts\Validation;
use Illuminate\Http\Request;

abstract class ApiBaseController extends Controller
{
    use DispatchesJobs;

    /**
     * HTTP header status code.
     *
     * @var int
     */
    protected $statusCode = 200;

    /**
     * Illuminate\Http\Request instance.
     *
     * @var Request
     */
    protected $request;

    /**
     * @var Validator
     */
    public $validator;


    public $auth;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
//        $this->validator = new Validation();

//        if($this->request->header("USER-TOKEN")){
//            $this->auth = Sentinel::getUserRepository()->findByPersistenceCode($this->request->header("USER-TOKEN"));
//        }
    }


    /**
     * Getter for statusCode.
     *
     * @return int
     */
    protected function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Setter for statusCode.
     *
     * @param int $statusCode Value to set
     *
     * @return self
     */
    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param  string $errorMessage
     * @param  int $errorCode
     * @param  null $statusCode
     * @param  array $headers
     * @return mixed
     */
    protected function respondWithErrorMessage($errorMessage, $errorCode = null, $statusCode = null, array $headers = [])
    {
        // if status code not change to error status => set it 400 error
        if (is_null($statusCode)) {
            $this->setStatusCode(400);
        } else {
            $this->setStatusCode($statusCode);
        }
        $parseErrors = array();
        $parseErrors[] = new ErrorFormat($errorMessage, $errorCode);

        $response = array(
            'error' => true,
            'data' => null,
            'errors' => $parseErrors
        );
        return response()->json($response, $this->statusCode, $headers);
    }


    /**
     * @param  \App\Common\ErrorFormat[] $errors
     * @param  null $statusCode
     * @param  array $headers
     * @return mixed
     */
    protected function respondWithError($errors, $statusCode = null, array $headers = [])
    {
        // if status code not change to error status => set it 400 error
        if (is_null($statusCode)) {
            $this->setStatusCode(400);
        } else {
            $this->setStatusCode($statusCode);
        }
        $parseErrors = array();
        foreach ($errors as $error) {
            $parseErrors[] = new ErrorFormat($error[0], $error[1]);
        }

        $response = array(
            'error' => true,
            'data' => null,
            'errors' => $parseErrors
        );
        return response()->json($response, $this->statusCode, $headers);
    }

    /**
     * @param    {array|object|string} $data
     * @param    array $headers
     * @return                         mixed
     */
    protected function respondWithSuccess($data, array $headers = [])
    {
        $this->setStatusCode(200);

        $response = array(
            'error' => false,
            'data' => $data,
            'errors' => null
        );

        return response()->json($response, $this->statusCode, $headers);
    }


    /**
     * Generate a Response with a 403 HTTP header and a given message.
     *
     * @param   string $message
     * @param   int $errorCode
     * @param    array $headers
     * @return  mixed
     */
    protected function errorForbidden($message = 'Forbidden', $errorCode = 0, array $headers = [])
    {
        return $this->respondWithErrorMessage($message, $errorCode, 403, $headers);
    }

    /**
     * Generate a Response with a 500 HTTP header and a given message.
     *
     * @param   string $message
     * @param   int $errorCode
     * @param    array $headers
     *
     * @return Response
     */
    protected function errorInternalError($message = 'Internal Error', $errorCode = 0, array $headers = [])
    {
        return $this->respondWithErrorMessage($message, $errorCode, 500, $headers);
    }

    /**
     * Generate a Response with a 404 HTTP header and a given message.
     *
     * @param   string $message
     * @param   int $errorCode
     * @param    array $headers
     *
     * @return Response
     */
    protected function errorNotFound($message = 'Resource Not Found', $errorCode = 0, array $headers = [])
    {
        return $this->respondWithErrorMessage($message, $errorCode, 404, $headers);
    }

    /**
     * Generate a Response with a 401 HTTP header and a given message.
     *
     * @param   string $message
     * @param   int $errorCode
     * @param    array $headers
     *
     * @return Response
     */
    protected function errorUnauthorized($message = 'Unauthorized', $errorCode = 0, array $headers = [])
    {
        return $this->respondWithErrorMessage($message, $errorCode, 401, $headers);
    }

    /**
     * Generate a Response with a 400 HTTP header and a given message.
     *
     * @param   string $message
     * @param   int $errorCode
     * @param    array $headers
     *
     * @return Response
     */
    protected function errorWrongArgs($message = 'Wrong Arguments', $errorCode = 0, array $headers = [])
    {
        return $this->respondWithErrorMessage($message, $errorCode, 400, $headers);
    }

    /**
     * Generate a Response with a 501 HTTP header and a given message.
     *
     * @param   string $message
     * @param   int $errorCode
     * @param    array $headers
     *
     * @return Response
     */
    protected function errorNotImplemented($message = 'Not implemented', $errorCode = 0, array $headers = [])
    {
        return $this->respondWithErrorMessage($message, $errorCode, 501, $headers);
    }

    /**
     * Parse error
     * */
    public function parseValidationError($validator){
        $errorCode = \Config::get('errorCode');
        $errorList = array();
        foreach ($validator->errors()->all() as $error) {
            $message = $errorCode[$errorCode['ApiErrorCodesFlip'][$error]];
            $errorList[] = array($message, $error);
        }
        return $errorList;
    }
}

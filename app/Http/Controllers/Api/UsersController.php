<?php namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Swagger\Annotations as SWG;
use App\Http\Controllers\ApiBaseController;
use App\Repositories\Eloquent\UserRepositoryEloquent;

/**
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   resourcePath="/User",
 *   description="User API",
 *   produces="['application/json']"
 * )
 *
 */
class UsersController extends ApiBaseController
{
    protected $userRepositoryEloquent;

    public function __construct(UserRepositoryEloquent $userRepositoryEloquent)
    {
        $this->userRepositoryEloquent = $userRepositoryEloquent;
    }

    /**
     * @SWG\Api(
     *   path="/api/user",
     *   @SWG\Operation(
     *     summary="Get list user",
     *     method="GET",
     *     nickname="getListUser",
     *     @SWG\Parameter(name="skip", description="Skip value", required=false, type="string", paramType="query", allowMultiple=false),
     *     @SWG\Parameter(name="take", description="Take value", required=false, type="string", paramType="query", allowMultiple=false),
     *     @SWG\Parameter(name="query", description="Query value ", type="string",required=false, paramType="query", allowMultiple=false, defaultValue=null),
     *     @SWG\Parameter(name="orderBy", description="Order by value format is '+firstname' or '-email'", type="string",required=false,paramType="query", allowMultiple=false, defaultValue=null),
     *     @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *     @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *     @SWG\ResponseMessage(code=404, message="Resource not found")
     *
     *   )
     * )
     */

    public function getList()
    {
        try {
            $results = $this->userRepositoryEloquent->all();
            return $this->respondWithSuccess($results);

        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }
    }

    /**
     * @SWG\Api(
     *   path="/api/user/{userId}",
     *   @SWG\Operation(
     *     summary="Get  user detail",
     *     method="GET",
     *     nickname="getDetail",
     *     @SWG\Parameter(name="userId", description="User ID", required=true, type="integer", paramType="path", allowMultiple=false),
     *     @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *     @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *     @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */

    public function getDetail($userId)
    {
        try {
//            $errorCode = \Config::get('errorCode');
//
//            $validation = Validator::make($userId,[
//                'userId' => 'required|numeric',
//            ], array([
//                'userId.num' => $errorCode['ApiErrorCode']['code_userId'],
//            ]));
//            if($validation->fails()){
//                $message = $this->parseValidationError($validation);
//                return $this->respondWithError($message,200);
//            }
            $results = $this->userRepositoryEloquent->find($userId);
            return $this->respondWithSuccess($results);
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }
    }

    /**
     * @SWG\Api(
     *     path="/api/user",
     *     @SWG\Operation(
     *      summary="Create user",
     *      method="POST",
     *      nickname="createUser",
     *     @SWG\Parameter(name="name", description="User name", required=true, type="string",paramType="query",allowMultiple=false),
     *     @SWG\Parameter(name="email", description="Email", required=true, type="string",paramType="query",allowMultiple=false),
     *     @SWG\Parameter(name="password", description="Password", required=true, type="string",paramType="query",allowMultiple=false),
     *     @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *     @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *     @SWG\ResponseMessage(code=404, message="Resource not found")
     * )
     * )
     *
     */
    public function createUser()
    {
        try {
            $errorCode = \Config::get('errorCode');
            //get input
            $var = array(
                'name' => Input::get('name'),
                'email' => Input::get('email'),
                'password' => \Hash::make(Input::get('password')),
            );
            //validate unique email
            //$validator = Validator::make($input, $rules, $messages);
            $validation = Validator::make($var, [
                'email' => 'unique:users,email|email|required',
                'name' => 'required',
                'password' => 'min:4|required',

            ],array(
                'email.unique' => $errorCode['ApiErrorCodes']['code_unique_email'],
                'name' => $errorCode['ApiErrorCodes']['code_name'],
                'password'  => $errorCode['ApiErrorCodes']['code_password']
            ));
            if($validation->fails()){
                $message = $this->parseValidationError($validation);
                return $this->respondWithError($message,200);
            }
            //insert user into db
//            $results = $this->userRepositoryEloquent->create($var);
            $results = \App\Models\User::create($var);

//            $results = User::firstOrCreate(['name'=> $name]);
            return $this->respondWithSuccess($results);


        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }

    }
    /**
     * @SWG\Api(
     *     path="/api/user/{userId}",
     *     @SWG\Operation(
     *      summary="Update user info.",
     *      method="PUT",
     *      nickname="updateUser",
     *     @SWG\Parameter(name="userId", description="User Id", required=true, type="integer",paramType="query",allowMultiple=false),
     *     @SWG\Parameter(name="new_email", description="New email", required=false, type="string",paramType="query",allowMultiple=false),
     *     @SWG\Parameter(name="new_name", description="New name", required=false, type="string",paramType="query",allowMultiple=false),
     *     @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *     @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *     @SWG\ResponseMessage(code=404, message="Resource not found")
     * ))
     */
    public function updateUser(){

        try {
            $errorCode = \Config::get('errorCode');
            $userId = Input::get('userId');
//            return $this->respondWithSuccess($userId);
            $var = array(
                'name' => Input::get('new_name'),
                'email' => Input::get('new_email'),
            );
            //validate unique email
            //$validator = Validator::make($input, $rules, $messages);
            $validation = Validator::make($var, [
                'email' => 'unique:users,email|required',
                'name' => 'required|max:20',
            ],array(
                'email.unique' => $errorCode['ApiErrorCodes']['code_unique_email'],
                'name' => $errorCode['ApiErrorCodes']['code_name'],
            ));

            if($validation->fails()){
                $message = $this->parseValidationError($validation);
                return $this->respondWithError($message,200);
            }
            //Update I
            $result = \App\Models\User::find($userId);
            $result->name = $var['name'];
            $result->email = $var['email'];
            $result->save();

            //Update II
//            $result = \App\Models\User::where('id',$userId)
//                                    ->update($var);

            return $this->respondWithSuccess($result);

        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }

    }

    /**
     * @SWG\Api(
     *     path="/api/user/{userId}",
     *     @SWG\Operation(
     *      summary="Delete user.",
     *      method="DELETE",
     *      nickname="deleteUser",
     *     @SWG\Parameter(name="userId", description="User Id", required=true, type="integer",paramType="path",allowMultiple=false),
     *     @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *     @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *     @SWG\ResponseMessage(code=404, message="Resource not found")
     * ))
     */
    public function deleteUser($userId){
        try {
            //Delete I
            $results = \App\Models\User::find($userId);
            $results->delete();
            //Delete II
//            $results = \App\Models\User::where('id',$userId)
//                                    ->delete();
            return $this->respondWithSuccess($results);
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }

    }

    /**
     * @SWG\Model(
     * 	id="j-create",
     * 	@SWG\Property(name="name", type="string", required=true, defaultValue="Johnny Depp"),
     * 	@SWG\Property(name="email", type="string", required=true, defaultValue="test@mail.com"),
     * 	@SWG\Property(name="password", type="string", required=true, defaultValue="******"),
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/user",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Create User using JSON",
     *      nickname="json-create",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="j-create", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function jCreate()
    {
        try {
            $errorCode = \Config::get('errorCode');
            //get input
            $var = array(
                'name' => Input::get('name'),
                'email' => Input::get('email'),
                'password' => \Hash::make(Input::get('password')),
            );
            //validate unique email
            //$validator = Validator::make($input, $rules, $messages);
            $validation = Validator::make($var, [
                'email' => 'unique:users,email|email|required',
                'name' => 'required',
                'password' => 'min:4|required',
            ],array(
                'email.unique' => $errorCode['ApiErrorCodes']['code_unique_email'],
                'name' => $errorCode['ApiErrorCodes']['code_name'],
                'password'  => $errorCode['ApiErrorCodes']['code_password']
            ));
            if($validation->fails()){
                $message = $this->parseValidationError($validation);
                return $this->respondWithError($message,200);
            }
            //Insert user
            $results = \App\Models\User::create($var);

            return $this->respondWithSuccess($results);

        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }
    }

    /**
     * @SWG\Model(
     * 	id="j-update",
     *  @SWG\Property(name="userId", type="string", required=true, defaultValue="98"),
     * 	@SWG\Property(name="new_name", type="string", required=true, defaultValue="Johnny Depp"),
     * 	@SWG\Property(name="new_email", type="string", required=true, defaultValue="test@mail.com"),
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/user/{userId}",
     *   @SWG\Operation(
     *      method="PUT",
     *      summary="Update user info using JSON",
     *      nickname="json-update",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="j-update", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function jUpdate(){
        try {
            $errorCode = \Config::get('errorCode');
            $userId = Input::get('userId');
//            return $this->respondWithSuccess($userId);
            $var = array(
                'name' => Input::get('new_name'),
                'email' => Input::get('new_email'),
//                'new_password' => \Hash::make(Input::get('new_password')),
            );
            //validate unique email
            //$validator = Validator::make($input, $rules, $messages);
            $validation = Validator::make($var, [
                'email' => 'unique:users,email|email|required',
                'name' => 'required|max:20',
//                'new_password' => 'required|min:4',
            ],array(
                'email.unique' => $errorCode['ApiErrorCodes']['code_unique_email'],
                'name' => $errorCode['ApiErrorCodes']['code_name'],
//                'new_password'  => $errorCode['ApiErrorCodes']['code_new_password']
            ));
//            return $this->respondWithSuccess($validation);

            if($validation->fails()){
                $message = $this->parseValidationError($validation);
                return $this->respondWithError($message,200);
            }
            //Update I
            $result = \App\Models\User::find($userId);
            $result->name = $var['name'];
            $result->email = $var['email'];
            $result->save();

            //Update II
//            $result = \App\Models\User::where('id',$userId)
//                                    ->update($var);

            return $this->respondWithSuccess($result);

        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }

    }

}



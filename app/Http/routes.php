<?php
Route::get('',function(){
$users = factory(App\Models\User::class, 13)->make()->toArray();
\DB::table('users')->insert($users);
dd($users);
});
Route::group([
    'namespace' => 'Api',
    'prefix' => 'api',
    'middleware' => ['cors']
], function () {
    Route::group([
        'prefix' => 'user'
    ], function () {
        Route::get('', 'UsersController@getList');
        Route::get('{userId}', 'UsersController@getDetail');
        Route::post('','UsersController@createUser');
        Route::put('{userId}','UsersController@updateUser');
        Route::delete('{userId}','UsersController@deleteUser');
        Route::post('','UsersController@jCreate');
        Route::put('{userId}','UsersController@jUpdate');
    });
});